$(document).ready(function () {
    $('.pricing-button').click(function () {
        $('#myModal').modal('show');
    });
});

$(document).ready(function () {
const userSlider = document.getElementById("userSlider");
const userCountDisplay = document.getElementById("userCount");

function highlightPlan(userCount) {
    $(".pricing-plan").removeClass("highlighted");
    
    $(".pricing-plan").each(function () {
        const min = parseInt($(this).data("range-min"));
        const max = parseInt($(this).data("range-max"));
        if (userCount >= min && userCount <= max) {
            $(this).addClass("highlighted");
            return false; 
        }
    });
}

 const freeDiv = document.getElementById("free");
 const proDiv = document.getElementById("pro");
 const enterprisediv=document.getElementById("enterprise")


userSlider.addEventListener("input", function () {
    const userCount = parseInt(userSlider.value);
    userCountDisplay.textContent = userCount;
    if(userSlider.value<=10){
        proDiv.style.boxShadow="0px 0px";
        enterprisediv.style.boxShadow="0px 0px";
       freeDiv.style.boxShadow='rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'
    }
    else if(userSlider.value<=20){
  freeDiv.style.boxShadow="0px 0px";
  enterprisediv.style.boxShadow="0px 0px";
    proDiv.style.boxShadow='rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'

    }
    else{
     freeDiv.style.boxShadow="0px 0px";
    proDiv.style.boxShadow="0px 0px";
    enterprisediv.style.boxShadow='rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'

    }
    console.log(userSlider.value,"slidervalue")
    highlightPlan(userCount);
});
});

// Submit Button
document.getElementById('submitButton').addEventListener('click', function() {
    var form = document.getElementById('myForm');
    form.submit();
});

(function () {
    function preventThrottle() {
        let isThrottled = false;
        return function (callback, delay) {
            if (!isThrottled) {
                isThrottled = true;
                callback();
                setTimeout(() => {
                    isThrottled = false;
                }, delay);
            }
        }
    }

    // Variables
    window.addEventListener('scroll', runOnScroll);
    const container = document.getElementById('imgGallery');
    const loader = document.getElementById('loader');
    let currentPage = 1;
    let isLoading = false;
    let totalPages = Infinity;

    // Initialization of Throttle Function
    const throttledSafeLoadMoreImage = preventThrottle();

    // Event-based functions
    function runOnScroll() {
        if (window.scrollY + window.innerHeight >= document.documentElement.scrollHeight - 1) {
            throttledSafeLoadMoreImage(loadMoreImage, 2000);
        }
    }

    // Function to load more images from your API
    function loadMoreImage() {
        if (isLoading || currentPage >= totalPages) {
            return;
        }

        isLoading = true;
        const apiUrl = `https://fakestoreapi.com/products?_page=${currentPage}&_limit=8`;

        loader.classList.remove('d-none');
        loader.classList.add('d-flex');

        async function fetchData() {
            try {
                const res = await fetch(apiUrl);
                const data = await res.json();

                data.forEach(item => {
                    const img = document.createElement('img');
                    img.src = item.image;
                    img.width = 300;
                    img.alt = item.title; // Use a suitable alt text
                    img.classList.add('col-12', 'col-md-6', 'col-lg-4', 'col-xl-3', 'my-3');
                    container.appendChild(img);
                });
                if (res.headers.has('X-Total-Pages')) {
                    totalPages = parseInt(res.headers.get('X-Total-Pages'));
                }

                currentPage++;
            } catch (error) {
                console.error('Error fetching data:', error);
            } finally {
                isLoading = false;
                loader.classList.add('d-none');
                loader.classList.remove('d-flex');
            }
        }

        fetchData();
    }
    const lazyLoadTrigger = document.getElementById('lazyLoadTrigger');
    const observer = new IntersectionObserver(
        (entries) => {
            if (entries[0].isIntersecting) {
                loadMoreImage();
            }
        },
        { threshold: 1 }
    );

    observer.observe(lazyLoadTrigger);
})();
